# PyCon Latam 2023

## Usando Servicios de Inteligencia Artificial con Python

![PyCon Latam 2023](images/pyconlatam2023.png)

El Jupyter Notebook de la charla es [pyconlatam2023.ipynb](pyconlatam2023.ipynb).

[pyconlatam2023_final.ipynb](pyconlatam2023_final.ipynb) es el Notebook ya "ejecutado", en el caso de que solo quieras ver el resultado final.

## Requirementos para ejecutar

- Python 3.8+
- Poetry
- Una cuenta de AWS con permisos para:
  - Comprehend
  - Polly
  - Rekognition
  - Transcribe
  - Translate

Nota: La capa gratuita de AWS soporta todas las llamadas sin que se tenga que hacer algún pago.

## Instrucciones

Ejecutar en la consola:

```bash
poetry install
poetry shell
jupyter notebook pyconlatam2023.ipynb
```

## Abrir en [Binder](https://mybinder.org)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/soldavid%2Fpyconlatam_2023/main?labpath=pyconlatam2023.ipynb)

## Thank you very much
